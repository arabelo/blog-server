'use strict';

const TIME_CLEAN_CACHE = 5 * 60 * 1000;     // 5 minutes
var cachedPostsInMemory = [];

module.exports = function(Post) {
  setInterval(cleanCache, TIME_CLEAN_CACHE);

  // Delimit the body field in maximum 150 characters
  Post.validatesLengthOf('body',
    {
      max: 150,
      message: {max: 'Maximum is 150 characters in body field'},
    });

  /**
  * Search for a text in Post's Title or Body
  * @param {string} text The text which will be searched in Post's Title or Body
  * @param {Function(Error)} callback
  */
  Post.search = function(text, callback) {
    // Create a pattern to be used in the search.
    // The Search method receives a text as parameter and this text will
    // be searched in Title or Body fields (in a case sensitive way).
    let pattern = new RegExp('.*' + text + '.*');

    let cachedPosts = itemsCached(text);
    if (cachedPosts !== null) {
      console.log('items got from cache ');
      callback(null, cachedPosts.posts);
    } else {
      // Find in DB the Posts that have 'text' in Title or Body fields
      Post.find({where: {or: [{title: {like: pattern}}, {body: {like: pattern}}]}},
        function(err, posts) {
          if (err) {
            console.error('error: ' + err);
            callback(null);
          }

          // Save the itemSearched and its results on the cache
          saveCachedItem(text, posts);

          console.log('items got from DB');
          callback(null, posts);
        }
      );
    }
  };

  /**
  * Get the cached items from memory
  * @param {string} key The text which was searched before
  * @return Returns the cached posts in the memory or null otherwise
  */
  function itemsCached(key) {
    let posts = cachedPostsInMemory.find(item => item.itemSearched === key);
    if (posts !== undefined)
      return posts;
    return null;
  }

  /**
  * Save a searched item and its values in the memory
  * @param {string} key The text which was searched before
  * @param {array} posts A list of posts that was retrieved in the search
  */
  function saveCachedItem(key, posts) {
    let itemCached = {
      itemSearched: key,
      date: new Date(),
      posts: posts,
    };
    cachedPostsInMemory.push(itemCached);
  }

  /**
  * Remove cached item that is older than the configured timer
  */
  function cleanCache() {
    console.log('time to clean the cache ');
    console.log('cache size before: ' + cachedPostsInMemory.length);
    let items = [];
    for (let item of cachedPostsInMemory) {
      let actualTime = new Date() - item.date;
      // Just save the items that are 'live' in cache, i.e., the time
      // that they were searched is shorter than the time defined by
      // TIME_CLEAN_CACHE
      if (actualTime < TIME_CLEAN_CACHE)
        items.push(item);
    }
    cachedPostsInMemory = items;
    console.log('cache size after: ' + cachedPostsInMemory.length);
  }
};
