# Blog Server

A simple backend service (REST API) to handle data about users and blog posts.

## Features

* CRUD operations to manage two entities: Users and Posts
* REST API to: create user, user login, edit user, get user by ID, create post, edit post, delete post, get post by ID and search for post using Title or Body fields.
* User Authentication

## Built With

Below is the list of technologies/applications that were used to built the backend service.

* [Node.js](https://nodejs.org/en/) - It is JavaScript runtime used to build scalable network applications. Version used: 8.9.1
* [Loopback](http://loopback.io/) - The Node.js framework used to create the REST API, to access the data from databases, access controls and model relationships. Version used: 2.22
* [MongoDB](https://www.mongodb.com/) - It is a NoSQL database with scalability and flexibility. Version used: 2.6.10
* [PM2](http://pm2.keymetrics.io/) - Process manager for Node.js applications. Used to deploy the application.

## Deployment

Two pages on the wiki were created to help install the necessary dependencies (see [this link](https://gitlab.com/arabelo/blog-server/wikis/home#tutorials)). You need to setup the environment in a Ubuntu (version 16.04) machine and after that you will be able to run the application.

If you are interested to run the application with PM2, use these commands below:

```bash
$> sudo npm install pm2 -g    #install the PM2
$> NODE_ENV=production pm2 start server/server.js -n blog-API   #run the app
```

You could see the application outputs in the log file available at `$HOME/.pm2.logs` directory.

Also you can monitor your application using PM2 commands. Try to use `pm2 help` to get a list of available commands.

## API Documentation

The REST API is documented using [Swagger Editor](https://swagger.io/swagger-editor/).

The documentation file is hosted at `doc/rest-api.yaml` in the application (Blog Server) source code. You might use the online editor to see the documentation. Just import the file on the online editor and you will see the REST API documentation.

This documentation is very useful for other developers who want to consume data from Blog Server REST API.

## Usage

As this application is a backend service, you need a client application to make REST requests to the API. In this case, you might use [Postman](https://www.getpostman.com/), [Curl](https://curl.haxx.se/) command line or use the [Loopback API explorer](https://loopback.io/doc/en/lb2/Use-API-Explorer.html) (which is used in development process).

Below are some examples using Curl:

* User Login

```bash
curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{\"username\": \"acosta\", \"password\": \"123456\"}" "http://localhost:3500/api/v1/users/login"

#Server response

{"id":"E7a7Yja3EfsfGHYZ3N9s1UM0WO55EZwtejMoUuNu0zFps4O3SjoLOU5gnxcBVaEJ","ttl":1209600,"created":"2017-11-15T02:16:35.026Z","userId":"5a071f1f874c685638b141c4"}
```
* Search a Text in Title or Body fields

```bash
curl -X GET --header "Accept: application/json" "http://localhost:3500/api/v1/posts/search?text=Test"

#Server response

[{"title":"Test Post","body":"Just a little test post","date":"2017-11-15T02:28:42.490Z","userId":"5a071f1f874c685638b141c4","id":"5a0ba65a100f62a2775c4fda"},{"title":"Test Post 2","body":"Just a second little test post","date":"2017-11-15T02:41:39.649Z","userId":"5a071f1f874c685638b141c4","id":"5a0ba963100f62a2775c4fdb"}]
```

If you want to see more examples about the API usage, you can access [this wiki page](https://gitlab.com/arabelo/blog-server/wikis/API_Usage). 
